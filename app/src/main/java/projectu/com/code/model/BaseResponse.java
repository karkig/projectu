package projectu.com.code.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by kailash on 18/2/18.
 */

public class BaseResponse {

    @SerializedName("error")
    @Expose
    public String error;
    @SerializedName("code")
    @Expose
    public Integer code;
    @SerializedName("text")
    @Expose
    public String text;

}