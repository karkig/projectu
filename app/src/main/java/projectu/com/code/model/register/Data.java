package projectu.com.code.model.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Data {
    @SerializedName("userid")
    @Expose
    public String userid;
}
