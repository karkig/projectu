package projectu.com.code.model.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import projectu.com.code.model.BaseResponse;

public class LoginResponse {
    @SerializedName("data")
    @Expose
    public Object data;

    @SerializedName("status")
    @Expose
    public BaseResponse status;

}
