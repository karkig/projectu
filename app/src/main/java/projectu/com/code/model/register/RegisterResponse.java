package projectu.com.code.model.register;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import projectu.com.code.model.BaseResponse;

public class RegisterResponse {

    @SerializedName("data")
    @Expose
    public Data data;
    @SerializedName("status")
    @Expose
    public BaseResponse status;

}