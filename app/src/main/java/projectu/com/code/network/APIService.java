package projectu.com.code.network;

import java.util.HashMap;
import java.util.Map;

import projectu.com.code.constants.Constants;
import projectu.com.code.model.BaseResponse;
import projectu.com.code.model.login.LoginResponse;
import projectu.com.code.model.register.RegisterResponse;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import rx.Observable;


public interface APIService {
    String LOGIN_URL = "login";
    String REGISTER_URL = "register";
    String UPLOAD_IMAGE_URL = "updateprofilepic";

/*

    @retrofit2.http.GET(Constants.SEARCh_YOUTUBE_VIDEO_URL)
    Observable<Response<VideoSearchResponse>> getVideoListFromYoutube(@QueryMap Map<String, String> params);
*/


    @FormUrlEncoded
    @retrofit2.http.POST(LOGIN_URL)
    Observable<Response<LoginResponse>> login(@FieldMap Map<String, Boolean> map);

    @FormUrlEncoded
    @retrofit2.http.POST(REGISTER_URL)
    Observable<Response<RegisterResponse>> register(@FieldMap Map<String, Object> map);

    @FormUrlEncoded
    @retrofit2.http.POST(UPLOAD_IMAGE_URL)
    Observable<Response<LoginResponse>> uploadPic(@FieldMap Map<String, String> map);


  /*  @Headers({
            "app_id:" + OxfordConfig.Application_ID + "",
            "app_key:" + OxfordConfig.Application_Key + "",
            "Accept: application/json"
    })
    @retrofit2.http.GET
    Observable<Response<WordInformationResponse>> getOxfordWordDetail(@Url String url);

*/
}