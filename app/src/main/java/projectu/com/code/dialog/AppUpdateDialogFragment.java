package projectu.com.code.dialog;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import projectu.com.code.R;
import projectu.com.code.activity.LoginActivity;

/**
 * Created by kailash on 3/22/2016.
 */
public class AppUpdateDialogFragment extends DialogFragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        setStyle(STYLE_NO_FRAME, R.style.FullScreenDialogStyle);

        super.onCreate(savedInstanceState);
        setCancelable(false);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View rootView = null;
        rootView = inflater.inflate(R.layout.include_created, container, false);
        TextView btnLogin = rootView.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(view -> {
            Intent intent = new Intent(getContext(), LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            dismiss();
        });
        return rootView;
    }
}
