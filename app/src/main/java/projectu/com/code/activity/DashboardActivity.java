package projectu.com.code.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.TableLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectu.com.code.R;
import projectu.com.code.adapters.SimpleFragmentPagerAdapter;


/**
 * A login screen that offers login via email/password.
 */
public class DashboardActivity extends AppCompatActivity {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    DashboardActivity context;
    int tabIcons[] = {R.drawable.home_tab, R.drawable.cart_tab, R.drawable.search_tab, R.drawable.user_tab};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        context = this;
        ButterKnife.bind(context);
        // Set up the login form.
        init();
    }

    private void init() {

        SimpleFragmentPagerAdapter adapter = new SimpleFragmentPagerAdapter(this, getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tbsLayout);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(tabIcons[0]);
        tabLayout.getTabAt(1).setIcon(tabIcons[1]);
        tabLayout.getTabAt(2).setIcon(tabIcons[2]);
        tabLayout.getTabAt(3).setIcon(tabIcons[3]);

    }


}

