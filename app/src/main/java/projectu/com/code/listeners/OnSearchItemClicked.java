package projectu.com.code.listeners;

/**
 * Created by kailash on 25/2/18.
 */

public interface OnSearchItemClicked {
    public void onItemClicked(String text);
}
