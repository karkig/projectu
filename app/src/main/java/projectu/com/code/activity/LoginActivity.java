package projectu.com.code.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectu.com.code.R;
import projectu.com.code.constants.Constants;
import projectu.com.code.model.login.LoginResponse;
import projectu.com.code.network.APIService;
import projectu.com.code.network.RequestResponses;
import projectu.com.code.util.AppLog;
import projectu.com.code.util.Util;
import projectu.com.code.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;


/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.loginBtn)
    ImageView loginBtn;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPass)
    EditText edtPass;
    @BindView(R.id.createAccount)
    LinearLayout createAccount;
    LoginActivity context;

    ProgressDialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context = this;
        ButterKnife.bind(context);
        init();
        clicks();
    }

    private void init() {
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage("Please wait...");
    }

    private void clicks() {
        createAccount.setOnClickListener(v -> {
            Intent intent = new Intent(context, CreateAccountActivity.class);
            startActivity(intent);
        });
        loginBtn.setOnClickListener(v -> {
            if (!Validator.isValidEmailAddress(edtEmail.getText().toString())) {
                Toast.makeText(context, "Please enter valid mail id", Toast.LENGTH_SHORT).show();
                return;
            }
            if (!Validator.isValidString(edtPass.toString())) {
                Toast.makeText(context, "Please enter password", Toast.LENGTH_SHORT).show();
                return;

            }
            Map<String, String> params = new HashMap<>();
            params.put("device", Constants.DEVICE);
            params.put("deviceId", Util.getDeviceId(context));
            params.put("email", edtEmail.getText().toString());
            params.put("password", edtPass.getText().toString());

            login(params);
        });
    }

    public void login(Map params) {
        dialog.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.login(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<LoginResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<LoginResponse> objectResponse) {
                        LoginResponse response = objectResponse.body();
                        dialog.dismiss();
                        if (response.status.code == 0) {
                            Intent intent = new Intent(context, DashboardActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {

                        }
                        Toast.makeText(context, response.status.text, Toast.LENGTH_SHORT).show();
                    }
                });
    }
}

