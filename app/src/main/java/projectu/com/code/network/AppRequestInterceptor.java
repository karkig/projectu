package projectu.com.code.network;


import java.util.concurrent.TimeUnit;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import projectu.com.code.constants.Constants;
import projectu.com.code.BuildConfig;

public class AppRequestInterceptor {

    public static OkHttpClient getRequestInterceptor(int state) {
        OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder();
        if (state == Constants.NORMAL_REQUEST && BuildConfig.DEBUG) {
            okHttpClient.addInterceptor(getLoggingInterceptor());
        }
        okHttpClient.readTimeout(2, TimeUnit.MINUTES);
        okHttpClient.connectTimeout(2, TimeUnit.MINUTES);
        return okHttpClient.build();
    }

    private static HttpLoggingInterceptor getLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }
}

