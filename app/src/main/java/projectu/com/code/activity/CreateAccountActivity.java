package projectu.com.code.activity;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.badoualy.stepperindicator.StepperIndicator;
import com.mvc.imagepicker.ImagePicker;

import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectu.com.code.R;
import projectu.com.code.constants.Constants;
import projectu.com.code.dialog.AppUpdateDialogFragment;
import projectu.com.code.model.login.LoginResponse;
import projectu.com.code.model.register.RegisterData;
import projectu.com.code.model.register.RegisterResponse;
import projectu.com.code.network.APIService;
import projectu.com.code.network.RequestResponses;
import projectu.com.code.util.AppLog;
import projectu.com.code.util.Util;
import projectu.com.code.util.Validator;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

import static projectu.com.code.util.Util.encodeImage;


/**
 * A login screen that offers login via email/password.
 */
public class CreateAccountActivity extends AppCompatActivity {

    @BindView(R.id.emailEdt)
    EditText emailEdt;
    @BindView(R.id.edtPAss)
    EditText edtPAss;
    @BindView(R.id.edtConfirmPass)
    EditText edtConfirmPass;

    @BindView(R.id.nextArrowCreateAccount)
    ImageView nextArrowCreateAccount;

    @BindView(R.id.includeCreateAcnt)
    LinearLayout includeCreateAcnt;
    @BindView(R.id.includeAge)
    LinearLayout includeAge;
    @BindView(R.id.edtAge)
    EditText edtAge;
    @BindView(R.id.edtWeight)
    EditText edtWeight;
    @BindView(R.id.edtPhone)
    EditText edtPhone;

    @BindView(R.id.radioMale)
    RadioButton radioMale;
    @BindView(R.id.includeWeight)
    LinearLayout includeWeight;
    @BindView(R.id.includeHeight)
    LinearLayout includeHeight;
    @BindView(R.id.includeUpload)
    LinearLayout includeUpload;
    @BindView(R.id.includeGender)
    LinearLayout includeGender;
    @BindView(R.id.includePaymentPlan)
    LinearLayout includePaymentPlan;
    @BindView(R.id.edtHeight)
    Spinner edtHeight;
    @BindView(R.id.nextArrowAge)
    ImageView nextArrowAge;
    @BindView(R.id.backArrowAge)
    ImageView backArrowAge;
    @BindView(R.id.backArrowGender)
    ImageView backArrowGender;
    @BindView(R.id.nextArrowGender)
    ImageView nextArrowGender;
    @BindView(R.id.imgUploadPic)
    ImageView imgUploadPic;
    @BindView(R.id.backArrowHeight)
    ImageView backArrowHeight;
    @BindView(R.id.radioGroupPlan)
    RadioGroup radioGroupPlan;
    @BindView(R.id.nextArrowHeight)
    ImageView nextArrowHeight;
    @BindView(R.id.backArrowWeight)
    ImageView backArrowWeight;
    @BindView(R.id.nextArrowWeight)
    ImageView nextArrowWeight;
    @BindView(R.id.backArrowUpload)
    ImageView backArrowUpload;
    @BindView(R.id.nextArrowUpload)
    ImageView nextArrowUpload;
    @BindView(R.id.backArrowPlan)
    ImageView backArrowPlan;
    @BindView(R.id.nextArrowPlan)
    ImageView nextArrowPlan;
    @BindView(R.id.imgStepper)
    ImageView imgStepper;
    @BindView(R.id.mainLayout)
    ImageView mainLayout;
    @BindView(R.id.radioFemale)
    RadioButton radioFemale;
    @BindView(R.id.edtReferal)
    EditText edtReferal;

    CreateAccountActivity context;
    RegisterData registerData = new RegisterData();
    ProgressDialog dialog;
    final int IMAGE_FROM_GALLERY = 1;
    String encodedImage = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);
        context = this;
        ButterKnife.bind(context);
        init();
        clicks();
    }

    private void init() {
        dialog = new ProgressDialog(context);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage("Please wait...");
        ArrayAdapter<String> adapter = getArrayAdapterFromArrayListForSpinner();
        edtHeight.setAdapter(adapter);
        edtHeight.setSelection(159);
    }

    private void clicks() {
        nextArrowCreateAccount.setOnClickListener(v -> {
            if (Validator.isValidEmailAddress(emailEdt.getText().toString())) {
                registerData.setEmail(emailEdt.getText().toString());
            } else {
                Toast.makeText(context, "Please enter valid mail id", Toast.LENGTH_SHORT).show();
                return;
            }
            if (edtConfirmPass.getText().toString().equals(edtPAss.getText().toString())) {
                registerData.setPassword(edtPAss.getText().toString());
                registerData.setConfirmPassword(edtConfirmPass.getText().toString());
            } else {
                Toast.makeText(context, "Password do not match", Toast.LENGTH_SHORT).show();
                return;
            }
           /* if (Validator.isValidMobile(emailEdt.getText().toString())) {
                registerData.setPhone(edtPhone.getText().toString());
            } else {
                Toast.makeText(context, "Please valid phone number", Toast.LENGTH_SHORT).show();
                return;
            }*/
            updateIndicator(R.drawable.step2);
            includeCreateAcnt.setVisibility(View.GONE);
            includeAge.setVisibility(View.VISIBLE);
            mainLayout.setImageResource(R.drawable.create_account_bg);
            hideKeyboardwithoutPopulate(emailEdt);

        });
        backArrowAge.setOnClickListener(v -> {
            includeCreateAcnt.setVisibility(View.VISIBLE);
            includeAge.setVisibility(View.GONE);
            mainLayout.setImageResource(R.drawable.create_account_bg);
            updateIndicator(R.drawable.step1);
        });
        nextArrowAge.setOnClickListener(v -> {
            int age = Validator.validInteger(edtAge.getText().toString());
            if (age > 3 && age < 100) {
                registerData.setAge(age + "");
                includeAge.setVisibility(View.GONE);
                includeGender.setVisibility(View.VISIBLE);
                mainLayout.setImageResource(R.drawable.gender);
                registerData.setAge(edtAge.getText().toString());
                updateIndicator(R.drawable.step3);
                hideKeyboardwithoutPopulate(edtAge);

            } else if (age == 0) {
                Toast.makeText(context, "Please enter valid age", Toast.LENGTH_SHORT).show();
                return;
            }
        });

        backArrowGender.setOnClickListener(v -> {
            includeAge.setVisibility(View.VISIBLE);
            mainLayout.setImageResource(R.drawable.create_account_bg);
            updateIndicator(R.drawable.step2);
            includeGender.setVisibility(View.GONE);
        });
        nextArrowGender.setOnClickListener(v -> {

            includeGender.setVisibility(View.GONE);
            includeHeight.setVisibility(View.VISIBLE);
            mainLayout.setImageResource(R.drawable.height);

            boolean male = radioMale.isSelected();
            registerData.setGender(male ? "Male" : "Female");
            updateIndicator(R.drawable.step4);

        });

        backArrowHeight.setOnClickListener(v -> {
            includeGender.setVisibility(View.VISIBLE);
            mainLayout.setImageResource(R.drawable.gender);
            includeHeight.setVisibility(View.GONE);
            updateIndicator(R.drawable.step3);
        });
        nextArrowHeight.setOnClickListener(v -> {
            registerData.setHeight(edtHeight.getSelectedItemPosition() + 1 + "");
            includeHeight.setVisibility(View.GONE);
            includeWeight.setVisibility(View.VISIBLE);
            mainLayout.setImageResource(R.drawable.body_weight);
            AppLog.e("--------- " + edtHeight.getSelectedItemPosition() + 1);
            updateIndicator(R.drawable.step5);
        });

        backArrowWeight.setOnClickListener(v -> {
            includeHeight.setVisibility(View.VISIBLE);
            includeWeight.setVisibility(View.GONE);
            mainLayout.setImageResource(R.drawable.height);

            updateIndicator(R.drawable.step4);

        });
        nextArrowWeight.setOnClickListener(v -> {
            includeWeight.setVisibility(View.GONE);
            includeUpload.setVisibility(View.VISIBLE);
            if (!edtWeight.getText().toString().equals("")) {
                registerData.setWeight(edtWeight.getText().toString());
            }
            updateIndicator(R.drawable.step6);
            hideKeyboardwithoutPopulate(edtWeight);
            mainLayout.setImageResource(R.drawable.create_account_bg);


        });

        backArrowUpload.setOnClickListener(v -> {
            includeWeight.setVisibility(View.VISIBLE);
            includeUpload.setVisibility(View.GONE);
            mainLayout.setImageResource(R.drawable.body_weight);

            updateIndicator(R.drawable.step5);
        });
        nextArrowUpload.setOnClickListener(v -> {
            includeUpload.setVisibility(View.GONE);
            includePaymentPlan.setVisibility(View.VISIBLE);
            edtReferal.setVisibility(View.GONE);
            updateIndicator(R.drawable.step7);
            mainLayout.setImageResource(R.drawable.create_account_bg);

        });
        backArrowPlan.setOnClickListener(v -> {
            includeUpload.setVisibility(View.VISIBLE);
            includePaymentPlan.setVisibility(View.GONE);
            edtReferal.setVisibility(View.GONE);
            updateIndicator(R.drawable.step6);

        });
        nextArrowPlan.setOnClickListener(v -> {

            int radioButtonID = radioGroupPlan.getCheckedRadioButtonId();
            View radioButton = radioGroupPlan.findViewById(radioButtonID);
            if (radioButton == null) {
                Toast.makeText(context, "Select a plan", Toast.LENGTH_SHORT).show();
                return;
            } else {
                String plan = (String) radioButton.getTag();
                if ("FREE".equals(plan)) {
                    registerData.setPlan("xyz");
                } else if ("ONE_MONTH".equals(plan)) {
                    registerData.setPlan("xyz");
                } else if ("THREE_MONTH".equals(plan)) {
                    registerData.setPlan("xyz");
                } else if ("SIX_MONTH".equals(plan)) {
                    registerData.setPlan("xyz");
                } else if ("ONE_YEAR".equals(plan)) {
                    registerData.setPlan("xyz");
                } else {
                    Toast.makeText(context, "Select a plan", Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            register();
        });
        imgUploadPic.setOnClickListener(v -> {
            Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(pickPhoto, IMAGE_FROM_GALLERY);
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    //  imageview.setImageURI(selectedImage);
                }

                break;
            case IMAGE_FROM_GALLERY:
                if (resultCode == RESULT_OK) {
                    Uri uri = imageReturnedIntent.getData();
                    final InputStream imageStream;
                    try {
                        imageStream = getContentResolver().openInputStream(uri);
                        final Bitmap selectedImage = BitmapFactory.decodeStream(imageStream);
                        encodedImage = encodeImage(selectedImage);
                        AppLog.e("------  encodeimage " + encodedImage);
                        // registerData.
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                    //   imageview.setImageURI(selectedImage);
                }
                break;
        }
    }

    void updateIndicator(int id) {
        try {
            imgStepper.setImageResource(id);
        } catch (Exception e) {
            AppLog.e("----- " + e.getMessage());
        }

    }

    private Map prepareData() {
        Map<String, Object> params = new HashMap<>();
        params.put("device", Constants.DEVICE);
        params.put("deviceid", Util.getDeviceId(context));
        params.put("email", registerData.getEmail());
        params.put("password", registerData.getPassword());
        params.put("confirm_password", registerData.getConfirmPassword());
        params.put("gender", registerData.getGender());
        params.put("body_weight", registerData.getWeight());
        params.put("plan_type", registerData.getPlan());
        params.put("phone", registerData.getPassword());
        params.put("age", registerData.getAge());
        params.put("height", registerData.getHeight());
        AppLog.e("-----------" + registerData.toString());
        return params;
    }

    public void register() {

        dialog.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.register(prepareData())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<RegisterResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();
                        AppLog.e("-------- " + throwable.getMessage());
                        Toast.makeText(context, "Email id already exist", Toast.LENGTH_SHORT).show();

                    }

                    @Override
                    public void onNext(Response<RegisterResponse> objectResponse) {
                        RegisterResponse response = objectResponse.body();
                        dialog.dismiss();
                        if (response.status.code == 0) {
                            if ("".equals(encodedImage)) {
                                registrationDone();
                            } else {
                                uploadPic(response.data.userid);
                            }
                        } else {

                            Toast.makeText(context, response.status.text, Toast.LENGTH_SHORT).show();

                        }

                    }
                });
    }

    private void registrationDone() {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        AppUpdateDialogFragment dialogFragment = new AppUpdateDialogFragment();
        dialogFragment.show(ft, "");
    }

    public void uploadPic(String userId) {

        Map<String, String> params = new HashMap<>();
        params.put("profilepic", encodedImage);
        params.put("userid", userId);
        dialog.show();
        APIService requestUrl = RequestResponses.getObservableServiceRequest(context, 0);
        requestUrl.uploadPic(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<Response<LoginResponse>>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        dialog.dismiss();
                        AppLog.e("-------- " + throwable.getMessage());
                    }

                    @Override
                    public void onNext(Response<LoginResponse> objectResponse) {
                        registrationDone();
                    }
                });
    }

    public ArrayAdapter<String> getArrayAdapterFromArrayListForSpinner() {
        ArrayAdapter<String> aArrayAdapter = new ArrayAdapter<String>(context, R.layout.spinner_layout);
        try {
            for (int i = 1; i < 300; i++) {
                aArrayAdapter.add(i + " cm  ");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aArrayAdapter;
    }

    public void hideKeyboardwithoutPopulate(EditText editText) {
        InputMethodManager imm = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(editText.getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }
}

