package projectu.com.code.activity;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jakewharton.rxbinding.view.RxView;

import java.util.HashMap;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import projectu.com.code.R;
import projectu.com.code.dialog.AppUpdateDialogFragment;
import projectu.com.code.model.BaseResponse;
import projectu.com.code.model.login.LoginResponse;
import projectu.com.code.network.APIService;
import projectu.com.code.network.RequestResponses;
import projectu.com.code.util.AppLog;
import retrofit2.Response;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


/**
 * A login screen that offers login via email/password.
 */
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.continueBtn)
    LinearLayout continueBtn;
    MainActivity context;
    @BindView(R.id.termAndCondition)
    TextView termAndCondition;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;
        ButterKnife.bind(context);
        clicks();
    }

    private void clicks() {
        continueBtn.setOnClickListener(v -> {
            Intent intent = new Intent(context, LoginActivity.class);
            startActivity(intent);
        });
        termAndCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }
}

