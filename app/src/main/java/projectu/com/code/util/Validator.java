package projectu.com.code.util;

import android.widget.EditText;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by kailash on 7/3/17.
 */

public class Validator {

    public static boolean isValidString(String value) {
        if (value != null && !value.contentEquals("null") && !value.equals("")) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean isValidInteger(Integer value) {
        if (value != null) {
            return true;
        } else {
            return false;
        }
    }

    public static int validInteger(Integer value) {
        if (value != null) {
            return value;
        } else {
            return 0;
        }
    }

    public static int validInteger(String value) {
        if (value != null) {
            int i;
            try {
                i = Integer.parseInt(value);
            } catch (Exception e) {
                i = 0;
            }
            return i;
        } else {
            return 0;
        }
    }

    public static String validString(String value) {

        if (value != null && !value.contentEquals("null") && !value.equals("")) {
            return value;
        } else {
            return "";
        }
    }


    public static long validLong(String timeStamp) {
        try {
            return Long.parseLong(timeStamp);
        } catch (Exception e) {
            return 0L;
        }
    }

    public static boolean isValidEmailAddress(String email) {
        String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
        Pattern p = Pattern.compile(ePattern);
        Matcher m = p.matcher(email);
        return m.matches();
    }

    public static boolean isValidMobile(String phone) {
        boolean check = false;
        if (!Pattern.matches("[a-zA-Z]+", phone)) {
            if (phone.length() < 6 || phone.length() > 13) {
                // if(phone.length() != 10) {
                check = false;
            } else {
                check = true;
            }
        } else {
            check = false;
        }
        return check;
    }
}
