package projectu.com.code.application;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.multidex.MultiDexApplication;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.core.display.SimpleBitmapDisplayer;

import projectu.com.code.util.AppLog;
import projectu.com.code.BuildConfig;
import projectu.com.code.R;


public class TFApplication extends MultiDexApplication {

    private static Context sContext;

    public static Context getAppContext() {
        return sContext;
    }


    @Override
    public void onCreate() {
        super.onCreate();
        sContext = this;
        initImageLoader(sContext);
        if (BuildConfig.DEBUG) {

        } else {
        }

    }

    private void initImageLoader(Context context) {
        try {
            DisplayImageOptions displayImageOptions = new DisplayImageOptions.Builder()
                    .cacheInMemory(true)
                    .cacheOnDisk(true)
                    .considerExifParams(true)
                   // .showImageOnLoading(R.drawable.logo)
                    //.showImageForEmptyUri(R.drawable.logo)
                    //.showImageOnFail(R.drawable.logo)
                    .bitmapConfig(Bitmap.Config.RGB_565)
                    .displayer(new SimpleBitmapDisplayer())
                    .build();

            ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(context);
            config.threadPriority(Thread.NORM_PRIORITY - 2);
            config.denyCacheImageMultipleSizesInMemory();
            config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
            config.diskCacheSize(200 * 1024 * 1024); // 50 MiB
            config.defaultDisplayImageOptions(displayImageOptions);
            config.tasksProcessingOrder(QueueProcessingType.LIFO);

            ImageLoader.getInstance().init(config.build());
        } catch (Exception e) {
            AppLog.e("Image Loading fail:" + e.getMessage());
        }
    }
}
